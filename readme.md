# Windows message events

This package is used to send and recieve messages between windows of the browser. There are some features which is applicable  only when the application running in the iframe.

## Installation

```bash
npm i windows-message
```

## Usage

```ts
import { WindowMessage } from 'windows-message';
```

##### `requestConfig`
This funcation can be used if the application is loaded inside iframe and it requires some  configuration from parent 
```ts
WindowMessage.requestConfig(hostURL, parent);
```

##### `requestExit`
If application is loaded in the iframe then child application can send request to close the  iframe window
```ts
WindowMessage.requestExit(hostURL, parent);
```

Here `hostURL` referes to the window, where the message to be sent. `parent` is  boolean, if passed then it will send message to the parent window.


##### `selectConfig`
Configuration will be recieved in the Observable provided like
```ts
WindowMessage.selectConfig(hostURL)
    .subscribe(event => {
        // Do something
    });
```
##### `selectEvents`
Child can also subscribe to all the events trigered
```ts
WindowMessage.selectEvents(hostURL)
    .subscribe(event => {
        // Do something
    });
```
