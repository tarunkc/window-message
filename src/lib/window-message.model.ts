export interface ClientMessageEvent {
    type: string;
    data: any;
    origin: string;
}

export interface IframeMessageEvent {
    type: string;
    data: any;
}