import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { WindowMessage } from './window-message';

describe('Tests for Window message testing', () => {

    describe('Check methods present', () => {
        it(`Methods should be present`, () => {
            expect(WindowMessage).toMatchObject({
                selectEvents: expect.any(Function),
                selectConfig: expect.any(Function),
                requestConfig: expect.any(Function),
                requestExit: expect.any(Function),
                sendMessage: expect.any(Function)
            });
        });
    });

    describe('Check methods executions', () => {
        it('Should return observable of all windows event', () => {
            expect(
                WindowMessage
                    .selectEvents()
                    .pipe(first()).toPromise()
            )
            .resolves.toBeInstanceOf(Observable)
        });
    
        it('Should return observable of config windows event', () => {
            expect(
                WindowMessage
                    .selectConfig()
                    .pipe(first()).toPromise()
            )
            .resolves.toBeInstanceOf(Observable)
        });
    });

    describe('Send message', () => {
        it('Send configuration request', () => {
            expect(
                WindowMessage.requestConfig('*', true)
            ).toBeUndefined()
        });
    
        it('Send request to close iframe', () => {
            expect(
                WindowMessage.requestExit('*', true)
            )
            .toBeUndefined()
        });
    });
});