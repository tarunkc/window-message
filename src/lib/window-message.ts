import { fromEvent, Observable } from "rxjs";
import { filter, map } from "rxjs/operators";
import { ClientMessageEvent } from "./window-message.model";

const ALL_HOST = '*';

/**
 * Get iframe  events
 * @param host Iframe url
 * @returns Observable  for parsed event data
 */
const selectEvents = (host = ALL_HOST): Observable<ClientMessageEvent> => {
    return fromEvent(window, 'message')
        .pipe(
            filter((e: Event):  boolean => (host === ALL_HOST || (e as MessageEvent).origin === host)),
            map((event: Event): ClientMessageEvent => {
                const { type, origin, data: eventData } = event as MessageEvent;
                let data: any;
                if (typeof eventData === 'string') {
                    try {
                        data = JSON.parse(eventData);
                    } catch (e) {
                        data = eventData;
                    }
                }
                if (!data) {
                    data = eventData;
                }
                return { type, origin, data };
            })
        );
}

/**
 * Get iframe config events
 * @param host Iframe url
 * @returns Observable for parsed config event data
 */
const selectConfig = (host = ALL_HOST): Observable<ClientMessageEvent> => {
    return selectEvents(host)
        .pipe(filter((event: ClientMessageEvent): boolean => event.data && event.data.type === 'config'));
}

/**
 * Send message to parent
 * @param message Message to be shared with parent
 * @param host Host url
 */
const sendMessage = (message: { [key: string]: unknown }, host = ALL_HOST, parent = false): void => {
    if (!window) {
        new Error('window is not defined');
        return;
    } else if (parent) {
        if (!window.parent) {
            new Error('Window is not running inside another application');
        } else {
            window.parent.postMessage(message, host);
        }
    } else {
        window.postMessage(message, host);
    }
};

/**
 * Request config from  parent
 * @param host Host url
 */
const requestConfig = (host = ALL_HOST, parent = false): void => sendMessage({ type: 'iframe:config' }, host, parent);

/**
 * Request exit iframe
 * @param host Host url
 */
const requestExit = (host = ALL_HOST, parent = false): void => sendMessage({ type: 'iframe:close' }, host, parent);

export const WindowMessage = {
    selectEvents,
    selectConfig,
    sendMessage,
    requestConfig,
    requestExit
};
